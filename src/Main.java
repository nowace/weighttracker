/*
 * Summer 2017
 * WeightTracker
 * Eric Nowac
 * Created 11/24/2017
 */

import java.io.File;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * @author nowace
 * @version 2017.11.24
 */
public class Main {

    public static void main(String[] args) {
        String appData = System.getenv("APPDATA");
        File appDataDirectory = new File(appData+"/WeightTracker");
        if(!appDataDirectory.exists()){
            appDataDirectory.mkdir();
        }
        String url = "jdbc:sqlite:"+ appDataDirectory.getPath() + "/test.db";
        try(Connection conn = DriverManager.getConnection(url)){
            if(conn != null){
                DatabaseMetaData meta = conn.getMetaData();
                System.out.println("The driver name is " + meta.getDriverName());
                System.out.println("A new database has been created.");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}
